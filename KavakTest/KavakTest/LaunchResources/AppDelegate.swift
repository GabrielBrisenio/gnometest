//
//  AppDelegate.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController: UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        GlobalSettings.shared.environment = .Development
        navController = UINavigationController(rootViewController: GnomesListRouter.create())
        navController?.navigationBar.tintColor = .purple
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        return true
    }
}

