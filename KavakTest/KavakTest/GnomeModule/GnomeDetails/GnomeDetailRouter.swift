//
//  GnomesListRouter.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

class GnomeDetailRouter {
    
    static func create(gnome: Gnome) -> UIViewController {
        let view = GnomeDetailView(gnome: gnome)
        let presenter: GnomeDetail_ViewToPresenterProtocol & GnomeDetail_InteractorToPresenterProtocol = GnomeDetailPresenter()
        let interactor: GnomeDetail_PresenterToInteractorProtocol = GnomeDetailInteractor()
        let router: GnomeDetail_PresenterToRouter = GnomeDetailRouter()
        
        view.presenter = presenter
        presenter._view = view
        presenter._interactor = interactor
        presenter._router = router
        interactor.presenter = presenter
        
        return view
    }
}

extension GnomeDetailRouter: GnomeDetail_PresenterToRouter {
    
}
