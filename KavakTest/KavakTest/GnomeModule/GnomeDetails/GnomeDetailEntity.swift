//
//  GnomesListEntity.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import Foundation

enum GnomeDetailSection: Int {
    case Picture = 0, PersonalData, AnotherData, Professions, Friends
}
