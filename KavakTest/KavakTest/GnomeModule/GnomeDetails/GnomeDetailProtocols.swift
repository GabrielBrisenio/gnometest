//
//  GnomesListProtocols.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import Foundation

protocol GnomeDetail_InteractorToPresenterProtocol: class {
    
}

protocol GnomeDetail_ViewToPresenterProtocol: class {
    var _view: GnomeDetail_PresenterToViewProtocol? { get set }
    var _interactor: GnomeDetail_PresenterToInteractorProtocol? { get set }
    var _router: GnomeDetail_PresenterToRouter? { get set }
}

protocol GnomeDetail_PresenterToViewProtocol: class {
    var presenter : GnomeDetail_ViewToPresenterProtocol? {set get}
}

protocol GnomeDetail_PresenterToInteractorProtocol: class {
    var presenter: GnomeDetail_InteractorToPresenterProtocol? {set get}
}

protocol GnomeDetail_PresenterToRouter: class {
    
}
