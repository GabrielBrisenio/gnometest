//
//  GnomeDetailsCell.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

class GnomeDetailsCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    convenience init(gnome: Gnome, cell_type: GnomeDetailSection, row: Int? = nil) {
        self.init(style: .default, reuseIdentifier: nil)

        selectionStyle = .none
        loadUIFor(gnome: gnome, cell_type: cell_type, row: row)
    }
    
    func loadUIFor(gnome: Gnome, cell_type: GnomeDetailSection, row: Int?) {
        switch cell_type {
            case .Picture:
                let pictureIV = UIImageView()
                pictureIV.sd_setImage(with: URL(string: gnome.thumbnail), placeholderImage: nil, options: .refreshCached, completed: nil)
                addSubview(pictureIV)
                pictureIV.addAnchorsAndCenter(centerX: true, centerY: true, width: 150, height: 150, left: nil, top: 20, right: nil, bottom: 20)
                pictureIV.layer.cornerRadius = 75
                pictureIV.layer.masksToBounds = true
            case .PersonalData:
                let mutable_attr = NSMutableAttributedString()
                let name_attr = NSAttributedString(string: gnome.name+"\n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.purple])
                let age_msg = gnome.age > 1 ? "\(gnome.age) years old" : " year old"
                let age_attr = NSAttributedString(string: age_msg, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
                mutable_attr.append(name_attr)
                mutable_attr.append(age_attr)
                
                let personalDataL = UILabel()
                personalDataL.textAlignment = .center
                personalDataL.numberOfLines = 0
                addSubview(personalDataL)
                personalDataL.addAnchorsWithMargin(0)
                personalDataL.attributedText = mutable_attr
                
            case .AnotherData:
                // Weight
                let other_data_mutable_attr = NSMutableAttributedString()
                let weight_concept_attr = NSAttributedString(string: "Weight: ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
                let weight_attr = NSAttributedString(string: String(format: "%0.2f kgs", arguments: [gnome.weight]), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
                
                // Height
                let height_concept_attr = NSAttributedString(string: " - Height: ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
                let height_attr = NSAttributedString(string: String(format: "%0.2f cm", arguments: [gnome.height]), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
                
                other_data_mutable_attr.append(weight_concept_attr)
                other_data_mutable_attr.append(weight_attr)
                other_data_mutable_attr.append(height_concept_attr)
                other_data_mutable_attr.append(height_attr)
                
                let otherDataL = UILabel()
                otherDataL.textAlignment = .center
                addSubview(otherDataL)
                otherDataL.addAnchorsWithMargin(0)
                otherDataL.attributedText = other_data_mutable_attr
            case .Professions:
                let professionL = UILabel()
                addSubview(professionL)
                professionL.addAnchorsWithMargin(20)
                professionL.text = gnome.professions[row ?? 0].rawValue
            case .Friends:
                let friendL = UILabel()
                addSubview(friendL)
                friendL.addAnchorsWithMargin(20)
                friendL.text = gnome.friends[row ?? 0]
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
