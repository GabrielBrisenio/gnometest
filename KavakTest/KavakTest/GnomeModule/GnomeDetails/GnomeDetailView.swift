//
//  GnomesListView.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

class GnomeDetailView: UIViewController {
    var presenter: GnomeDetail_ViewToPresenterProtocol?
    var tableV: UITableView?
    private var gnome: Gnome?
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }

    
    convenience init(gnome: Gnome) {
        self.init(nibName: nil, bundle: nil)
        
        self.gnome = gnome
    }
    
    required init?(coder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUI()
    }
    
    func loadUI() {
        title = "Profile"
    
        tableV = UITableView(frame: .zero, style: .grouped)
        tableV?.delegate = self
        tableV?.dataSource = self
        view.addSubview(tableV!)
        tableV?.addAnchorsWithMargin(0)
    }
}

// MARK: TableView Delegate
extension GnomeDetailView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch GnomeDetailSection(rawValue: section) {
            case .Picture: return 1
            case .PersonalData: return 1
            case .AnotherData: return 1
            case .Professions: return gnome?.professions.count ?? 0
            case .Friends: return gnome?.friends.count ?? 0
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let _gnome = gnome else { return UITableViewCell() }
    
        switch GnomeDetailSection(rawValue: indexPath.section) {
            case .Picture: return GnomeDetailsCell(gnome: _gnome, cell_type: .Picture)
            case .PersonalData: return GnomeDetailsCell(gnome: _gnome, cell_type: .PersonalData)
            case .AnotherData: return GnomeDetailsCell(gnome: _gnome, cell_type: .AnotherData)
            case .Professions: return GnomeDetailsCell(gnome: _gnome, cell_type: .Professions, row: indexPath.row)
            case .Friends: return GnomeDetailsCell(gnome: _gnome, cell_type: .Friends, row: indexPath.row)
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch GnomeDetailSection(rawValue: section) {
            case .Picture: return ""
            case .PersonalData: return "Personal data"
            case .AnotherData: return "Other data"
            case .Professions: return "My professions"
            case .Friends: return "My friends"
        default: return nil
        }
    }
}

// MARK: GnomeDetail_PresenterToViewProtocol
extension GnomeDetailView: GnomeDetail_PresenterToViewProtocol {
   
}
