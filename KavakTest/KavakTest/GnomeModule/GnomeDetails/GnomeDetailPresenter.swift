//
//  GnomesListPresenter.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import Foundation

class GnomeDetailPresenter: GnomeDetail_ViewToPresenterProtocol {
    var _view: GnomeDetail_PresenterToViewProtocol?
    var _interactor: GnomeDetail_PresenterToInteractorProtocol?
    var _router: GnomeDetail_PresenterToRouter?

}

extension GnomeDetailPresenter: GnomeDetail_InteractorToPresenterProtocol {
    
}
