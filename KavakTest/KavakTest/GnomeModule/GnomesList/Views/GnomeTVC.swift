//
//  GnomeTVC.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit
import SDWebImage

class GnomeTVC: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    convenience init(gnome: Gnome) {
        self.init(style: .default, reuseIdentifier: nil)
        
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        
        let pictureIV = UIImageView()
        pictureIV.backgroundColor = .gray
        pictureIV.sd_setImage(with: URL(string: gnome.thumbnail), placeholderImage: nil, options: .refreshCached)
        addSubview(pictureIV)
        pictureIV.addAnchorsAndSize(width: 100, height: 100, left: 20, top: 20, right: nil, bottom: 20)
        pictureIV.layer.cornerRadius = 50
        pictureIV.layer.masksToBounds = true
        
        let nameL = UILabel()
        nameL.text = gnome.name
        addSubview(nameL)
        nameL.addAnchors(left: 20, top: 20, right: 20, bottom: nil, withAnchor: .left, relativeToView: pictureIV)
        
        let ageL = UILabel()
        ageL.text = gnome.age > 1 ? "\(gnome.age) years old" : " year old"
        addSubview(ageL)
        ageL.addAnchors(left: 20, top: nil, right: 20, bottom: nil, withAnchor: .left, relativeToView: pictureIV)
        ageL.addAnchors(left: nil, top: 5, right: nil, bottom: nil, withAnchor: .top, relativeToView: nameL)
        
        let professionsL = UILabel()
        professionsL.text = gnome.professions.count > 1 ? "\(gnome.professions[0]) and more" : gnome.professions.count == 1 ? gnome.professions[0].rawValue : "without profession"
        addSubview(professionsL)
        professionsL.addAnchors(left: 20, top: nil, right: 20, bottom: nil, withAnchor: .left, relativeToView: pictureIV)
        professionsL.addAnchors(left: nil, top: 5, right: nil, bottom: nil, withAnchor: .top, relativeToView: ageL)
        
        let friendsL = UILabel()
        friendsL.text = gnome.friends.count < 2 ? "has just one friend" : "has \(gnome.friends.count) friends"
        addSubview(friendsL)
        friendsL.addAnchors(left: 20, top: nil, right: 20, bottom: nil, withAnchor: .left, relativeToView: pictureIV)
        friendsL.addAnchors(left: nil, top: 5, right: nil, bottom: nil, withAnchor: .top, relativeToView: professionsL)
        
        professionsL.textColor = .lightGray
        professionsL.font = UIFont(name: "", size: UIFont.smallSystemFontSize)
        friendsL.textColor = .lightGray
        friendsL.font = UIFont(name: "", size: UIFont.smallSystemFontSize)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
