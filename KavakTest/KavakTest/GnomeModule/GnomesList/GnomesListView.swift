//
//  GnomesListView.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

class GnomesListView: UIViewController {
    var presenter: GnomesList_ViewToPresenterProtocol?
    var tableV: UITableView?
    var loader: Loader?
    var gnomes: [Gnome]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUI()
        loadData()
    }
    
    func loadData() {
        presenter?.getGnomes()
    }
    
    func loadUI() {
        title = "Suggestions"
        loader = Loader(superView: view, heightForRows: nil)
    
        tableV = UITableView()
        tableV?.delegate = self
        tableV?.dataSource = self
        view.addSubview(tableV!)
        tableV?.addAnchorsWithMargin(0)
    }
}

// MARK: TableView Delegate
extension GnomesListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gnomes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let _gnome = gnomes else { return UITableViewCell() }
        
        return GnomeTVC(gnome: _gnome[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let gnome = gnomes?[indexPath.row] else { return }
        presenter?.showMoreDetailsFor(navController: navigationController, gnome: gnome)
    }
}

// MARK: GnomesList_PresenterToViewProtocol
extension GnomesListView: GnomesList_PresenterToViewProtocol {
    func didFinishWith(error: GnomeError) {
        
    }
    
    func didFinishWith(gnomes: [Gnome]) {
        self.gnomes = gnomes
        tableV?.reloadData()
    }
    
    
}
