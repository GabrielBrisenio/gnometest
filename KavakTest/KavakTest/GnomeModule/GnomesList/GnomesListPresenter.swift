//
//  GnomesListPresenter.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit.UINavigationController

class GnomesListPresenter: GnomesList_ViewToPresenterProtocol {
    var _view: GnomesList_PresenterToViewProtocol?
    var _interactor: GnomesList_PresenterToInteractorProtocol?
    var _router: GnomesList_PresenterToRouter?
    
    func getGnomes() {
        (_view as? GnomesListView)?.loader?.start()
        _interactor?.getGnomes()
    }
    
    func showMoreDetailsFor(navController: UINavigationController?, gnome: Gnome) {
        _router?.showMoreDetailsFor(navController: navController, gnome: gnome)
    }
}

extension GnomesListPresenter: GnomesList_InteractorToPresenterProtocol {
    func didFinishWith(gnomes: [Gnome]) {
        (_view as? GnomesListView)?.loader?.stop()
        _view?.didFinishWith(gnomes: gnomes)
    }
    
    func didFinishWith(error: GnomeError) {
        (_view as? GnomesListView)?.loader?.stop()
        _view?.didFinishWith(error: error)
    }
}
