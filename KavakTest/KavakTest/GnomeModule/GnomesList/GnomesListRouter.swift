//
//  GnomesListRouter.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

public class GnomesListRouter {
    
    public static func create() -> UIViewController {
        let view = GnomesListView()
        let presenter: GnomesList_ViewToPresenterProtocol & GnomesList_InteractorToPresenterProtocol = GnomesListPresenter()
        let interactor: GnomesList_PresenterToInteractorProtocol = GnomesListInteractor()
        let router: GnomesList_PresenterToRouter = GnomesListRouter()
        
        view.presenter = presenter
        presenter._view = view
        presenter._interactor = interactor
        presenter._router = router
        interactor.presenter = presenter
        
        GlobalSettings.shared.base_url = GlobalSettings.shared.environment.getBaseURL()
        
        return view
    }
}

extension GnomesListRouter: GnomesList_PresenterToRouter {
    func showMoreDetailsFor(navController: UINavigationController?, gnome: Gnome) {
        navController?.pushViewController(GnomeDetailRouter.create(gnome: gnome), animated: true)
    }
}
