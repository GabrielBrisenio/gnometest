//
//  GnomesListInteractor.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import Foundation

class GnomesListInteractor: NSObject, GnomesList_PresenterToInteractorProtocol {
    var presenter: GnomesList_InteractorToPresenterProtocol?
    var dataTask: URLSessionDataTask?
    var responseData: Data?
    
    
    func getGnomes() {
        if dataTask != nil { dataTask?.cancel() }
        
        guard let url_str = (GlobalSettings.shared.base_url+"master/data.json").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            PrintManager.print("getGnomes method: url_str nil")
            presenter?.didFinishWith(error: .OtherError)
            return
        }
        guard let url = URL(string: url_str) else {
            PrintManager.print("getGnomes method: url nil")
            presenter?.didFinishWith(error: .OtherError)
            return
        }
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: .main)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        responseData = Data()
        
        dataTask = session.dataTask(with: request)
        
        #warning("Agregué este timer, solo para que vean el shimmer en acción, ya que la respuesta es demasiado rápida y no se aprecia esta parte del código")
        //dataTask?.resume()
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { [weak self] (timer) in
            timer.invalidate()
            self?.dataTask?.resume()
        }
        
    }
}

// MARK: URLSession Delegate
extension GnomesListInteractor: URLSessionDelegate, URLSessionDataDelegate  {
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        guard let httpResponse = response as? HTTPURLResponse else {
            presenter?.didFinishWith(error: .OtherError)
            return
        }
        
        if httpResponse.statusCode == 200 {
            completionHandler(URLSession.ResponseDisposition.allow)
        } else {
            PrintManager.print("HTTPURLResponse with error: \(httpResponse.statusCode)")
            presenter?.didFinishWith(error: .OtherError)
            completionHandler(URLSession.ResponseDisposition.cancel)
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        responseData?.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error == nil {
            guard let data = responseData else {
                PrintManager.print("Data object was nil")
                presenter?.didFinishWith(error: .OtherError)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(ResponseData.self, from: data)
                presenter?.didFinishWith(gnomes: model.brastlewark)
            } catch let event {
                PrintManager.print(event.localizedDescription)
                presenter?.didFinishWith(error: .OtherError)
            }
        } else {
            PrintManager.print(error?.localizedDescription ?? "Error nil")
            presenter?.didFinishWith(error: .OtherError)
        }
    }
}
