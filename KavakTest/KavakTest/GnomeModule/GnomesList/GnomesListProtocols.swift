//
//  GnomesListProtocols.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

protocol GnomesList_InteractorToPresenterProtocol: class {
    func didFinishWith(error: GnomeError)
    func didFinishWith(gnomes: [Gnome])
}

protocol GnomesList_ViewToPresenterProtocol: class {
    var _view: GnomesList_PresenterToViewProtocol? { get set }
    var _interactor: GnomesList_PresenterToInteractorProtocol? { get set }
    var _router: GnomesList_PresenterToRouter? { get set }
    
    func getGnomes()
    func showMoreDetailsFor(navController: UINavigationController?, gnome: Gnome)
}

protocol GnomesList_PresenterToViewProtocol: class {
    var presenter : GnomesList_ViewToPresenterProtocol? {set get}
    
    func didFinishWith(error: GnomeError)
    func didFinishWith(gnomes: [Gnome])
}

protocol GnomesList_PresenterToInteractorProtocol: class {
    var presenter: GnomesList_InteractorToPresenterProtocol? {set get}
    
    func getGnomes()
}

protocol GnomesList_PresenterToRouter: class {
    func showMoreDetailsFor(navController: UINavigationController?, gnome: Gnome)
}
