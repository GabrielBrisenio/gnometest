//
//  GlobalExtensions.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

extension UILabel {
    static func loader() -> UILabel {
        let label = UILabel()
        label.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        label.layer.cornerRadius = 5
        label.layer.masksToBounds = true
        
        return label
    }
}

public class PrintManager {
    public static func print(_ message: String?...) {
        #if DEBUG
            message.forEach({debugPrint($0 ?? "")})
        #endif
    }
}
