//
//  GnomeLoader.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import UIKit

public class Loader: UIView, UITableViewDataSource, UITableViewDelegate {
    private weak var superView: UIView?
    private var tableView: UITableView?
    private var heightForRows: [CGFloat]?
    private var indicator: UIActivityIndicatorView?
    
    public init(superView: UIView, heightForRows: [CGFloat]?) {
        super.init(frame: superView.frame)
        self.superView = superView
        self.heightForRows = heightForRows
    }
    
    public func start() {
        backgroundColor = .clear
        superView?.addSubview(self)
        
        tableView = UITableView()
        tableView?.backgroundColor = .white
        tableView?.dataSource = self
        tableView?.delegate = self
        tableView?.separatorColor = .clear
        tableView?.isUserInteractionEnabled = false
        addSubview(tableView!)
        addAnchorsWithMargin(0)
        tableView?.addAnchorsWithMargin(0)
        
        tableView?.reloadData()
    }
    
    public func stop() {
        tableView?.removeFromSuperview()
        tableView = nil
        self.removeFromSuperview()
    }
    
    // MARK: - TableView Protocol
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let _heightForRows = heightForRows {
            if _heightForRows.count > indexPath.row {
                return LoaderCell(withHeight: _heightForRows[indexPath.row]+20)
            } else { return LoaderCell(withHeight: 135.0) }
        } else { return LoaderCell(withHeight: 135.0) }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _heightForRows = heightForRows {
            if _heightForRows.count > indexPath.row {
                return _heightForRows[indexPath.row]+20
            } else { return 100 }
        } else { return 100 }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LoaderCell: UITableViewCell {
    
    var background: UIView?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        selectionStyle = .none
        
        background = UIView()
        background?.backgroundColor = .white
        background?.layer.cornerRadius = 10
        addSubview(background!)
    }
    
    convenience init(withHeight height: CGFloat) {
        self.init(style: .default, reuseIdentifier: nil)
        
        backgroundColor = .clear
        background?.addAnchorsAndSize(width: kWidth-20, height: height+20, left: 10, top: 10, right: 10, bottom: 10)
        background?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        let firstL = UILabel.loader()
        background?.addSubview(firstL)
        firstL.addAnchorsAndSize(width: kWidth/2-20, height: 200, left: 0, top: 0, right: 0, bottom: 0)
        
        background?.addShimmerEffect()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

