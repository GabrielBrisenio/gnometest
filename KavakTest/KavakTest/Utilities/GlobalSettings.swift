//
//  GlobalSettings.swift
//  KavakTest
//
//  Created by Gabriel Briseño on 12/03/21.
//

import Foundation

enum Environment {
    case Release
    case Qa
    case Development
    
    func getBaseURL() -> String {
        switch self {
            case .Development: return "https://raw.githubusercontent.com/rrafols/mobile_test/"
            case .Qa: return "https://raw.githubusercontent.com/rrafols/mobile_test/"
            case .Release: return "https://raw.githubusercontent.com/rrafols/mobile_test/"
        }
    }
}

class GlobalSettings {
    static let shared = GlobalSettings()
    var environment: Environment = .Development
    var base_url = ""
}
